class Validator {
  constructor(formSelector) {
    this.formElement = document.querySelector(formSelector);
  }

  validate(options) {
    let isValid = true;
    options.forEach((option) => {
      let inputEl = this.formElement.querySelector(option.inputSelector);
      let errorEl = inputEl.parentElement.querySelector(".form-message");

      errorEl.innerText = "";
      inputEl.parentElement.classList.remove("invalid");

      let inputValue = inputEl.value;

      for (let i = 0; i < option.rules.length; i++) {
        if (this[option.rules[i]](inputValue, option.name)) {
          isValid = false;
          errorEl.innerText = this[option.rules[i]](inputValue, option.name);
          errorEl.style.display = "block";
          inputEl.parentElement.classList.add("invalid");
          break;
        }
      }
    });
    return isValid;
  }

  isRequired(value, name) {
    return value.trim() ? undefined : `${name} can not be blank`;
  }

  isValidEmail(value, name) {
    const regex =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return regex.test(value) ? undefined : `${name} is not valid`;
  }

  isValidLink(value, name) {
    let regex =
      /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
    return regex.test(value) ? undefined : `${name} is not valid`;
  }

  isNumber(value, name) {
    return !isNaN(value) ? undefined : `${name} must be a number`;
  }
}

let validator = new Validator("#productInput");

export const isValidNewProduct = () => {
  return validator.validate([
    {
      name: "Name",
      inputSelector: "#inputName",
      rules: ["isRequired"],
    },
    {
      name: "Price",
      inputSelector: "#inputPrice",
      rules: ["isRequired", "isNumber"],
    },
    {
      name: "Screen",
      inputSelector: "#inputScreen",
      rules: ["isRequired"],
    },
    {
      name: "Back camera",
      inputSelector: "#inputBackCamera",
      rules: ["isRequired"],
    },
    {
      name: "Front camera",
      inputSelector: "#inputFrontCamera",
      rules: ["isRequired"],
    },
    {
      name: "Image Link",
      inputSelector: "#inputImage",
      rules: ["isRequired", "isValidLink"],
    },
    {
      name: "Description",
      inputSelector: "#inputDescription",
      rules: ["isRequired"],
    },
    {
      name: "Type",
      inputSelector: "#inputType",
      rules: ["isRequired"],
    },
  ]);
};
