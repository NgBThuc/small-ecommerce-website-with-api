import { resetGetRequestQuery } from "./constants/constants";
import { renderFilterList } from "./controller/filterList.controller";
import { resetForm, showProductOnForm } from "./controller/form.controller";
import {
  deleteProduct,
  getFilterProductsData,
  getProductsFromAPI,
  getSearchData,
  saveNewProduct,
  sortProducts,
  updateProduct,
} from "./controller/products.controller";
import { renderProductsTable } from "./controller/productsTable.controller";
import { isValidNewProduct } from "./validation/validation";

const newProductEl = document.querySelector(".newProduct");
const newProductBtn = document.querySelector("#newProductBtn");
const newProductCancelBtn = document.querySelector("#newProductCancelBtn");
const filterEl = document.querySelector(".search__filter");
const filterBtn = document.querySelector("#searchFilterBtn");
const loaderEl = document.querySelector("#loader");
const saveProductBtn = document.querySelector("#saveProductBtn");
const productsTableBodyEl = document.querySelector("#productsTableBody");
const productsTableHeadEl = document.querySelector("#productsTableHead");
const newProductTitleEl = newProductEl.querySelector("h2");
const searchBtnEl = document.querySelector("#searchBtn");
const searchInputEl = document.querySelector("#searchInput");
const menuBtnEl = document.querySelector("#menuBtn");
const headerNavEl = document.querySelector("#headerNav");
const closeMenuBtnEl = document.querySelector("#closeMenuBtn");

let isEdit = false;
let currentProductId = "";

const togglePopup = (popUpEl) => {
  let currentState = popUpEl.dataset.popupToggle;
  popUpEl.dataset.popupToggle = currentState === "hide" ? "show" : "hide";
};

const showLoader = () => {
  loaderEl.style.display = "grid";
};

const hideLoader = () => {
  loaderEl.style.display = "none";
};

const getProductsAndRender = async () => {
  let productsData = await getProductsFromAPI();
  renderProductsTable(productsData);
  renderFilterList(productsData);
};

const resetTableFilterState = (tableRowEl, tableHeadEl) => {
  tableRowEl.querySelectorAll("th").forEach((element) => {
    if (element.dataset.sortBy !== tableHeadEl.dataset.sortBy) {
      element.removeAttribute("data-sort-order");
    }
  });
};

const changeTableFilterState = (tableHeadEl) => {
  switch (tableHeadEl.dataset.sortOrder) {
    case undefined:
      tableHeadEl.setAttribute("data-sort-order", "asc");
      break;
    case "asc":
      tableHeadEl.dataset.sortOrder = "desc";
      break;
    default:
      tableHeadEl.removeAttribute("data-sort-order");
  }

  let sortOrder = tableHeadEl.dataset.sortOrder;
  let sortBy = tableHeadEl.dataset.sortBy;

  return [sortOrder, sortBy];
};

menuBtnEl.addEventListener("click", () => {
  headerNavEl.dataset.toggle = "show";
});

closeMenuBtnEl.addEventListener("click", () => {
  headerNavEl.dataset.toggle = "hide";
});

newProductBtn.addEventListener("click", () => {
  newProductTitleEl.innerText = "Input New Product";
  togglePopup(newProductEl);
});

newProductCancelBtn.addEventListener("click", () => {
  togglePopup(newProductEl);
});

filterBtn.addEventListener("click", () => {
  togglePopup(filterEl);
});

filterEl.addEventListener("change", async (event) => {
  let filterValue = event.target.value;
  togglePopup(filterEl);

  if (filterValue === "false") {
    event.target.checked = false;
    resetGetRequestQuery();
    getProductsAndRender();
    return;
  }

  let productsData = await getFilterProductsData(filterValue);
  renderProductsTable(productsData);
});

saveProductBtn.addEventListener("click", async (event) => {
  event.preventDefault();

  if (!isValidNewProduct()) {
    return;
  }

  if (isEdit === true) {
    await updateProduct(currentProductId);
  } else {
    await saveNewProduct();
  }

  resetGetRequestQuery();
  await getProductsAndRender();
  togglePopup(newProductEl);
  resetForm();
  isEdit = false;
  currentProductId = "";
});

productsTableHeadEl.addEventListener("click", async (event) => {
  if (
    !event.target.closest("th") ||
    !event.target.closest("th").dataset.sortBy
  ) {
    return;
  }

  let tableRowEl = event.target.closest("tr");
  let tableHeadEl = event.target.closest("th");

  resetTableFilterState(tableRowEl, tableHeadEl);

  let [sortOrder, sortBy] = changeTableFilterState(tableHeadEl);

  let sortedProductsData = await sortProducts(sortOrder, sortBy);

  renderProductsTable(sortedProductsData);
});

productsTableBodyEl.addEventListener("click", async (event) => {
  let productId = event.target.closest("tr").id;

  if (!event.target.closest("button")) {
    return;
  }

  if (event.target.closest("button").dataset.buttonType !== "delete") {
    return;
  }

  await deleteProduct(productId);
  await getProductsAndRender();
});

productsTableBodyEl.addEventListener("click", async (event) => {
  let productId = event.target.closest("tr").id;

  if (!event.target.closest("button")) {
    return;
  }

  if (event.target.closest("button").dataset.buttonType !== "edit") {
    return;
  }

  isEdit = true;
  currentProductId = productId;
  newProductTitleEl.innerText = "Adjust Product";
  await showProductOnForm(productId);
  togglePopup(newProductEl);
});

searchBtnEl.addEventListener("click", async () => {
  let productsData = await getSearchData();
  renderProductsTable(productsData);
  searchInputEl.value = "";
});

getProductsAndRender();

export { showLoader, hideLoader, togglePopup };
