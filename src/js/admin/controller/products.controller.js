import { default as axios } from "axios";
import { hideLoader, showLoader } from "../admin";
import { API_URL, getRequestQuery } from "../constants/constants";
import { getDataFromForm } from "./form.controller";

const searchInputEl = document.querySelector("#searchInput");

const getProductsFromAPI = async () => {
  let querys = [];
  for (let key in getRequestQuery) {
    if (getRequestQuery[key]) {
      querys.push(`${key}=${getRequestQuery[key]}`);
    }
  }
  showLoader();
  let { data } = await axios.get(`${API_URL}/Products?${querys.join("&")}`);
  hideLoader();
  return data;
};

const saveProductToAPI = async (product) => {
  try {
    await axios.post(`${API_URL}/Products`, product);
  } catch (error) {
    console.log(error);
  }
};

const getSearchData = async () => {
  try {
    getRequestQuery.search = searchInputEl.value;
    let data = getProductsFromAPI();
    return data;
  } catch (error) {
    console.log(error);
  }
};

const getFilterProductsData = async (filterValue) => {
  getRequestQuery.type = filterValue;
  let data = await getProductsFromAPI();
  return data;
};

const saveNewProduct = async () => {
  let newProduct = getDataFromForm();
  showLoader();
  await saveProductToAPI(newProduct);
  hideLoader();
};

const deleteProduct = async (productId) => {
  showLoader();
  try {
    await axios.delete(`${API_URL}/Products/${productId}`);
  } catch (error) {
    console.log(error);
  }
  hideLoader();
};

const updateProduct = async (productId) => {
  let updatedProduct = getDataFromForm();
  showLoader();
  await axios.put(`${API_URL}/Products/${productId}`, updatedProduct);
  hideLoader();
};

const sortProducts = async (sortOrder, sortBy) => {
  getRequestQuery.sortBy = sortBy;
  getRequestQuery.order = sortOrder;
  let data = await getProductsFromAPI();
  return data;
};

export {
  getProductsFromAPI,
  getDataFromForm,
  saveNewProduct,
  deleteProduct,
  updateProduct,
  getSearchData,
  getFilterProductsData,
  sortProducts,
};
