const filterListEl = document.querySelector(".search__filterList");

export const renderFilterList = (productsData) => {
  const uniqueTypes = [...new Set(productsData.map((product) => product.type))];
  let htmlMarkup = "";

  uniqueTypes.forEach((type) => {
    htmlMarkup += `
      <div class="search__filterItem">
        <input type="radio" name="brand" id="${type.toLowerCase()}" value="${type.toLowerCase()}" />
        <label for="${type.toLowerCase()}">${type}</label>
      </div>
    `;
  });

  filterListEl.innerHTML = htmlMarkup;
};
