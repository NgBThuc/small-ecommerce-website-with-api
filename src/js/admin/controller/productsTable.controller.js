const productsTableBodyEl = document.querySelector("#productsTableBody");

export const renderProductsTable = (productsData) => {
  let htmlMarkup = "";

  productsData.forEach((product) => {
    htmlMarkup += `
      <tr id="${product.id}">
        <td>
          <div>
            <button data-button-type="edit"><i class="fa fa-edit"></i></button>
            <button data-button-type="delete"><i class="fa fa-times-circle"></i></button>
          </div>
        </td>
        <td><div>${product.id}</div></td>
        <td title="${product.name}"><div>${product.name}</div></td>
        <td><div>${product.price}$</div></td>
        <td title="${product.screen}"><div>${product.screen}</div></td>
        <td title="${product.backCamera}"><div>${product.backCamera}</div></td>
        <td title="${product.frontCamera}"><div>${product.frontCamera}</div></td>
        <td>
          <div>
            <img
              class="img-fluid"
              src="${product.img}"
              alt=""
            />
          </div>
        </td>
        <td title="${product.desc}">
          <div>${product.desc}</div>
        </td>
        <td><div>${product.type}</div></td>
      </tr>
    `;
  });

  productsTableBodyEl.innerHTML = htmlMarkup;
};
