import axios from "axios";
import { hideLoader, showLoader } from "../admin";
import { API_URL } from "../constants/constants";

export const getDataFromForm = () => {
  const name = document.querySelector("#inputName").value;
  const price = document.querySelector("#inputPrice").value;
  const screen = document.querySelector("#inputScreen").value;
  const backCamera = document.querySelector("#inputBackCamera").value;
  const frontCamera = document.querySelector("#inputFrontCamera").value;
  const img = document.querySelector("#inputImage").value;
  const desc = document.querySelector("#inputDescription").value;
  const type = document.querySelector("#inputType").value;

  return { name, price, screen, backCamera, frontCamera, img, desc, type };
};

export const showProductOnForm = async (productId) => {
  showLoader();
  const response = await axios.get(`${API_URL}/Products/${productId}`);
  hideLoader();
  const product = response.data;

  document.querySelector("#inputName").value = product.name;
  document.querySelector("#inputPrice").value = product.price;
  document.querySelector("#inputScreen").value = product.screen;
  document.querySelector("#inputBackCamera").value = product.backCamera;
  document.querySelector("#inputFrontCamera").value = product.frontCamera;
  document.querySelector("#inputImage").value = product.img;
  document.querySelector("#inputDescription").value = product.desc;
  document.querySelector("#inputType").value = product.type;
};

export const resetForm = () => {
  document.querySelector("#inputName").value = "";
  document.querySelector("#inputPrice").value = "";
  document.querySelector("#inputScreen").value = "";
  document.querySelector("#inputBackCamera").value = "";
  document.querySelector("#inputFrontCamera").value = "";
  document.querySelector("#inputImage").value = "";
  document.querySelector("#inputDescription").value = "";
  document.querySelector("#inputType").value = "";
};
