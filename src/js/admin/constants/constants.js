export const API_URL = "https://62db6ca3d1d97b9e0c4f336e.mockapi.io";

export let getRequestQuery = {
  search: "",
  type: "",
  sortBy: "",
  order: "",
};

export const resetGetRequestQuery = () => {
  getRequestQuery = {
    search: "",
    type: "",
    sortBy: "",
    order: "",
  };
};
