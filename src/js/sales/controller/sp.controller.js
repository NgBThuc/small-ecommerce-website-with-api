export let renderDSSP = function (dssp) {
  var contentHTML = "";

  dssp.forEach((sp) => {
    var contentDiv = `<div class="product__item">
        <div class="product__item-img">
        <img src="${sp.img}" alt="${sp.name}" />
        </div>
        <div class="product__item-detail">
          <h2>${sp.name}</h2>
          <h3><span>Giá: </span>$${sp.price}</h3>
          <p title="Màn hình: ${sp.screen}"><span>Màn hình: </span>${sp.screen}</p>
          <p title="Camera trước: ${sp.backCamera}"><span>Camera trước: </span>${sp.backCamera}</p>
          <p title="Camera sau: ${sp.frontCamera}"><span>Camera sau: </span>${sp.frontCamera}</p>
          <p class="product__desc" title="Mô tả: ${sp.desc}"><span>Mô tả: </span>${sp.desc}</p>
        </div>
        <div class="product__cart-btn">
        <button onclick="addToCart('${sp.id}')">Add to cart</button>
        </div>
        </div>`;

    contentHTML += contentDiv;
  });

  document.getElementById(`product__list`).innerHTML = contentHTML;
};

export let renderCart = function (dssp) {
  let contentHTML = "";
  let sum = 0;
  let orderId = Math.floor(Math.random() * (999 - 100 + 1) + 100);
  let cartNumber = 0;

  dssp.forEach((sp) => {
    var contentTr = `
    <tr>
    <td>
      <img src="${sp.img}" alt="san pham">
    </td>
    <td>${sp.name}</td>
    <td class="btn__volumn-wrap">
    <button id="btn__volumn" onclick="decreaseNumber(${sp.id})"><i class="fa-solid fa-caret-left"></i></button>
    <p>${sp.quantity}</p>
    <button id="btn__volumn" onclick="increaseNumber(${sp.id})"><i class="fa-solid fa-caret-right"></i></button>
    </td>
    <td>$${sp.price}</td>
    <td>
    <button onclick="deleteCartItem(${sp.id})"><i class="fa-solid fa-trash"></i></button>
    </td>
  </tr>`;

    sum += sp.total();
    cartNumber += sp.quantity;
    contentHTML += contentTr;
  });

  document.getElementById(`cartbody_tbody`).innerHTML = contentHTML;
  document.getElementById(`totalcart`).innerHTML = sum;
  document.getElementById(`numberCart`).innerHTML = cartNumber;
  document.getElementById(`order__id`).innerHTML = orderId;
  document.getElementById(`verity__tbody`).innerHTML = contentHTML;

  if (document.getElementById(`cartbody_tbody`).innerHTML == "") {
    document.getElementById(`cartbody_tbody`).innerHTML =
      "Không có sản phẩm trong giỏ hàng";
  }
};

export let renderVerifyCart = function (dssp) {
  let contentHTML = "";
  let sum = 0;
  let orderId = Math.floor(Math.random() * (999 - 100 + 1) + 100);
  let cartNumber = 0;

  dssp.forEach((sp) => {
    var contentTr = `
    <tr>
    <td>
      <img src="${sp.img}" alt="san pham">
    </td>
    <td>${sp.name}</td>
    <td>${sp.quantity}</td>
    <td class="verify__price">$${sp.price}</td>
  </tr>`;
    sum += sp.total();
    cartNumber += sp.quantity;
    contentHTML += contentTr;
  });
  console.log(sum);
  document.getElementById(`purchase__total`).innerHTML = sum;
  document.getElementById(`verify__total`).innerHTML = sum;
  document.getElementById(`order__id`).innerHTML = orderId;
  document.getElementById(`verity__tbody`).innerHTML = contentHTML;
};
