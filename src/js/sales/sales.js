import axios from "axios";
import {
  renderCart,
  renderDSSP,
  renderVerifyCart,
} from "./controller/sp.controller.js";
import { CartItem } from "./model/cartItem.model.js";

const BASE_URL = "https://62db6ca3d1d97b9e0c4f336e.mockapi.io";

let CART_LOCALSTORAGE = "CART_LOCALSTORAGE";
let DSSPVERIFY_LOCALSTORAGE = "DSSPVERIFY_LOCALSTORAGE";

let dssp = [];
let cart = JSON.parse(localStorage.getItem(CART_LOCALSTORAGE)) || [];

if (cart) {
  cart = cart.map((sp) => {
    return new CartItem(sp.id, sp.name, sp.img, sp.price, sp.quantity);
  });
  renderCart(cart);
}

const getProductAndRender = async () => {
  onLoading();
  let { data } = await axios.get(`${BASE_URL}/Products`);
  dssp = data;
  offLoading();
  renderDSSP(dssp);
  renderBrand(dssp);
};

const renderBrand = (dssp) => {
  const brands = [...new Set(dssp.map((sp) => sp.type))];
  let htmlMarkup = '<option value="">Choose Brand</option>';
  brands.forEach((brand) => {
    htmlMarkup += `
      <option value="${brand.toLowerCase()}">${brand}</option>
    `;
  });
  document.querySelector("#selectbrand").innerHTML = htmlMarkup;
};

getProductAndRender();

document.getElementById("selectbrand").addEventListener("change", (e) => {
  const selectedItem = e.target.value;
  let result = dssp.filter((sp) => sp.type.toLowerCase() === selectedItem);
  if (!selectedItem) {
    result = dssp;
  }
  renderDSSP(result);
});

function addToCart(id) {
  let selectedSp = dssp.find((sp) => sp.id === id);
  let cartItem = new CartItem(
    selectedSp.id,
    selectedSp.name,
    selectedSp.img,
    selectedSp.price,
    (selectedSp.quantity = 1)
  );

  let index = cart.findIndex((item) => item.id === id);

  if (index === -1) {
    cart.push(cartItem);
  } else {
    cart[index].quantity++;
  }

  localStorage.setItem(CART_LOCALSTORAGE, JSON.stringify(cart));
  renderCart(cart);
}
window.addToCart = addToCart;

function deleteCartItem(id) {
  cart = cart.filter((item) => item.id != id);
  localStorage.setItem(CART_LOCALSTORAGE, JSON.stringify(cart));
  renderCart(cart);
  if (cart.length === 0) {
    document.getElementById(`cartbody_tbody`).innerHTML =
      "Không có sản phẩm trong giỏ hàng";
  }
}
window.deleteCartItem = deleteCartItem;

function decreaseNumber(id) {
  let index = cart.findIndex((item) => item.id == id);
  if (cart[index].quantity <= 1) {
    return;
  }
  cart[index].quantity--;
  localStorage.setItem(CART_LOCALSTORAGE, JSON.stringify(cart));
  renderCart(cart);
}
window.decreaseNumber = decreaseNumber;

function increaseNumber(id) {
  let index = cart.findIndex((item) => item.id == id);
  cart[index].quantity++;
  localStorage.setItem(CART_LOCALSTORAGE, JSON.stringify(cart));
  renderCart(cart);
}
window.increaseNumber = increaseNumber;

document.getElementById(`cart`).addEventListener("click", () => {
  document.getElementById(`cartshow`).style.display = "block";
});

document.getElementById(`cart__close`).addEventListener("click", () => {
  return (document.getElementById(`cartshow`).style.display = "none");
});

document.getElementById(`purchase__btn`).addEventListener("click", () => {
  if (
    document.getElementById(`cartbody_tbody`).innerHTML !== "" &&
    document.getElementById(`verity__tbody`).innerHTML !== ""
  ) {
    document.getElementById(`cart__close`).click();
    document.getElementById(`verify`).style.display = "block";
    let verifyCart = cart;
    cart = [];
    localStorage.setItem(CART_LOCALSTORAGE, JSON.stringify(cart));
    renderCart(cart);

    localStorage.setItem(DSSPVERIFY_LOCALSTORAGE, JSON.stringify(verifyCart));
    renderVerifyCart(verifyCart);
  }
});

document.getElementById(`clear__btn`).addEventListener("click", () => {
  cart = [];
  localStorage.setItem(CART_LOCALSTORAGE, JSON.stringify(cart));
  renderCart(cart);
});

document.getElementById(`order`).addEventListener("click", () => {
  cart = [];
  localStorage.setItem(CART_LOCALSTORAGE, JSON.stringify(cart));
  renderCart(cart);
  document.getElementById(`verify`).style.display = "none";
  document.getElementById(`purchase`).style.display = "block";
});

document.getElementById(`cancel`).addEventListener("click", () => {
  return (document.getElementById(`verify`).style.display = "none");
});

document.getElementById(`confirm`).addEventListener("click", () => {
  return (document.getElementById(`purchase`).style.display = "none");
});

function onLoading() {
  document.getElementById("loading").style.display = "flex";
}

function offLoading() {
  document.getElementById("loading").style.display = "none";
}
