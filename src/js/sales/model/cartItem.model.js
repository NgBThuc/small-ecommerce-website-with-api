export class CartItem {
  constructor(id, name, img, price, quantity) {
    this.id = id;
    this.name = name;
    this.img = img;
    this.price = price;
    this.quantity = quantity;
    this.total = () => {
      return this.price * this.quantity;
    };
  }
}
